import * as React from 'react';
import './Register.scss';


export class Register extends React.Component {
   render() {
    return (
      <section className="Register">
        <div className="container"> 
     <form className='form'>  
        <div className="form-field">
          <label>Create Username</label>
          <input id="username" type="username" placeholder="Username" />
        </div>
        <div  className="form-field">
          <label>Create Password</label>
          <input id="password" type="password" placeholder="Password" />
        </div>
        <div  className="form-field">
          <input type="submit"  className="btn btn-signin" value="Register" />
        </div>
      </form>
      </div>
      </section>
    );
  }
}


export default Register;
