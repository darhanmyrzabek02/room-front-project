import * as React from 'react';
import './Login.scss';
// import { alertConstants } from '../_constants';
// export const alertActions = {
//   success,
//   error,
//   clear
// }
// ;

export class Login extends React.Component {


  


  
  //  success(message) {
  //     return { type: alertConstants.SUCCESS, message };
  // }
  
  //  error(message) {
  //     return { type: alertConstants.ERROR, message };
  // }
  
  //  clear() {
  //     return { type: alertConstants.CLEAR };
  // }

   render() {
    return (
      <section className="Login">
        <div  className="container"> 
     <form  className='form'>  
        <div  className="form-field">
          <label>Email</label>
          <input id="email" type="email" placeholder="Email" />
        </div>
        <div  className="form-field">
          <label>Password</label>
          <input id="password" type="password" placeholder="Password" />
        </div>
        <div  className="form-options">
          <div  className="checkbox-field">
            <input id="rememberMe" type="checkbox"  className="checkbox" />
            <label >Remember Me</label>
          </div>
          <a href="/Register">Create Account?</a>
        </div>
        <div  className="form-field">
          <input type="submit"  className="btn btn-signin" value="Submit" />
        </div>
      </form>
      </div>
      </section>
    );
  }
}


export default Login;
