import React, { Component } from 'react';
//import app from '../../App'
//import RoomsList from '../Room/RoomsList';
//import {Route} from 'react-router-dom';

import './Home.scss';
class Home extends Component {
  render() {
    return (
      <>
        <div className="hero" id="home">
          <div className="background"></div>
          <div className="overlay"></div>
          <div className="hero-content">
            <h1>MEETINGLY</h1>
            <div className="line"></div>
            <p>Smart and simple meeting room booking system </p>
            <button className="showVid">BOOK ROOM NOW</button>
          </div>
          <a className="readMore smooth-scroll" href="#about">
            <span className="fa fa-angle-down"></span>
          </a>
        </div>
        <div className="mobile-nav">
          <h1>MEETINGLY</h1>
          <span className="fa fa-bars"></span>
        </div>
        <nav>
          <div id="nav-wrapper">
            <div className="nav-left">
              <h1>MEETINGLY</h1>
            </div>
            <div className="nav-right">
              <ul>
                <li>
                  <a className="smooth-scroll" href="#home">
                    Home
                  </a>
                </li>
                <li>
                  <a className="smooth-scroll" href="#about">
                    About
                  </a>
                </li>
                <li>
                  <a className="smooth-scroll" href="#">
                    The Team
                  </a>
                </li>
                <li>
                  <a className="smooth-scroll" href="#">
                    Sign out
                  </a>
                </li>
             
                
              </ul>
            </div>
          </div>
        </nav>
      </>
    );
  }
}

export default Home;
