import React from 'react';
//import FormInput from '../ui-kit/FormInput';

//import {Form,RangePicker} from 'antd';
import { Form, Input, Button, Radio } from 'antd';
import DatePicker from 'antd/es/date-picker'; 
import 'antd/dist/antd.css'; 
import './BookForm.scss';



const BookForm = (props)=>{
  
 
      
     const handleSubmit = (values) => {
      
       
       let data = {
          roomId:props.id,
          userId: 2,
          Subject : values.Subject,
          StartTime: values.time[0].toDate(),
          EndTime: values.time[0].toDate(),
       
       }
      console.log(data);
        
        
        fetch('http://localhost:3000/meetings', {
          method: 'POST',
          headers: {
            'Accept':'application/json',
            'Content-type':'application/json',
          },

          body: JSON.stringify(data),
        });
      }
    
  

    return (
      <div className="meeting-form">
      <Form onFinish={ handleSubmit } layout='horizontal'>
          
         
           <Form.Item
              name="Subject"
              required={true}
              placeholder="Enter title"
              label="title"
          >  
          <Input type='text'/>
          </Form.Item>
          
        
            {/* <Form.Item label="choose start time" name='StartTime'>
            <DatePicker showTime format="YYYY-MM-DD HH:mm:ss"  />
            </Form.Item>
            
            <Form.Item label="choose end time" name='EndTime'>
            <DatePicker showTime format="YYYY-MM-DD HH:mm:ss" />
            </Form.Item> */}

            <Form.Item label='choose time' name="time">
              <DatePicker.RangePicker showTime format="YYYY-MM-DD HH:mm:ss"/>
            </Form.Item>
            
            
              <Form.Item>
          <Button type="primary" htmlType="submit">Submit</Button>
              </Form.Item>
     
     
          </Form>



        </div>
    )
}
export default BookForm;