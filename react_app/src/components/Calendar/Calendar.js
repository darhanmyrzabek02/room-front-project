import * as React from 'react';
import {
  ScheduleComponent,
  Week,
  Day,
  Month,
  Inject,
  ViewsDirective,
  ViewDirective,
  Agenda,
} from '@syncfusion/ej2-react-schedule';
//import { DataManager, WebApiAdaptor } from '@syncfusion/ej2-data';



class Calendar extends React.Component {

    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         events: [],
    //         slotStep: 1
    //     };
    // }

//   state = {
//     data: [
//       {
//         id: 1,
//         Subject: 'Create Service',
//         roomId: 1,
//         StartTime: '2020-03-16T07:00:00.000Z',
//         EndTime: '2020-03-16T08:00:00.000Z',
//       },
//     ],
//   };

  //    private localData:EventSettingsModel = {
  //         dataSource:[{
  //         Id: 2,
  //         Subject: 'Paris',
  //         StartTime: new Date(2020, 3, 15, 10, 0),
  //         EndTime: new Date(2020, 3, 15, 12, 30),

  //         }]

  //          };


  

//   checkAvailability = (newEvent) => {
//     const oldEvents = this.state.events;
//     const sameDayEvents = oldEvents.filter(event => moment(event.start).format("YYYY/MM/DD") === moment(newEvent.start).format("YYYY/MM/DD"));
//     const timeComparison = sameDayEvents.filter(event => ((newEvent.end > event.start && newEvent.start < event.end) && event.oldEvent));

//     timeComparison.length > 0 ? newEvent.slotAvailable = false : newEvent.slotAvailable = true;
//     return newEvent;
// };
onSelectEvent(pEvent) {
  const r = window.confirm("Would you like to remove this event?")
  if(r === true){
    
    this.setState((prevState, props) => {
      const events = [...prevState.events]
      const idx = events.indexOf(pEvent)
      events.splice(idx, 1);
      return { events };
    });
  }
}


  //     private remoteData = new DataManager({
  //         url: 'https://js.syncfusion.com/demos/ejservices/api/Schedule/LoadData',
  //         adaptor: new WebApiAdaptor,
  //         crossDomain: true
  //     });




  render() {
    console.log("from calendar",this.props.meetingsData);
    
   const  localData = {
    dataSource: this.props.meetingsData,
     // dataSource:this.state.data
     };

    return (
        
    //     <button
    //     className="btn btn--block room__btn"
    //     onClick={
    //       //  () => this.props.setModalIsOpen(true)
    //       () => getID(props.item.id)
    //     }
    //   >
    //     Reserve
    //   </button>

    
   
      <ScheduleComponent
        width="100%"
        height="550px"
        currentView="Month"
        selectedDate={new Date(2020, 3, 5)}
        eventSettings={localData}
        onSelectEvent = {event => this.onSelectEvent(event)} //Fires selecting existing event
        
        
      >
        <ViewsDirective>
          <ViewDirective option="Week" dateFormat="dd-MMM-yyyy" />
          <ViewDirective option="Month" showWeekend={false}  />

          <ViewDirective
            option="Agenda"
            eventSettings={localData}
            allowVirtualScrolling={false}
          />
        </ViewsDirective>

        <Inject services={[Week, Month, Day, Agenda]} />
      </ScheduleComponent>
    );
  }
}
// const mapStateToProps = state => ({
//     meetingsData: state.meetings.meetingsData,
//   });

export default Calendar;
