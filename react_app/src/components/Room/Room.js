import React from 'react';
import './RoomsList';

// import { connect } from 'react-redux';
// import { setMeetings } from './../../redux/actions/meeting.actions';
// import { getMeetings } from './../../redux/effects/meeting.effects';



const Room = (props) => {




   const getID=(clicked_id)=> {
    //  console.log(clicked_id);
   
     props.setid(clicked_id)
     props.setModalIsOpen(true)
   //  props.getMeetings(clicked_id+'hello');
    // console.log()


    }

  //   state = {

  //     selectedId: clicked_id,
  // }
  // handleId = () => {
  //     var id = this.clicked_id;
  //     this.props.onSelectedId(id);
  // }

  return (
    <li className="Room__item">
      <div className="room">
        <div className="room__image">
          <img src={'http://localhost:3000/' + props.item.imageUrl} />
        </div>
        <div className="room__content">
          <div className="room__title">{props.item.name}</div>

          <div className="room__title">Capacity of room : {props.item.capacity}</div>

          <button
            className="btn btn--block room__btn"
            onClick={
              //  () => this.props.setModalIsOpen(true)
              () => getID(props.item.id)
            }
          >
            Reserve
          </button>
        </div>
      </div>
    </li>
  );
};
// const mapStateToProps = state => ({
//     meetingsData: state.meetings.meetingsData,
//   });
// export default connect(mapStateToProps,{getMeetings})(Room);
 export default Room;
